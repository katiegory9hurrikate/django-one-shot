from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {'todos': todos,}
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {'todo_list': todo_list}
    return render(request, 'todos/detail.html', context)

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/create.html', {'form': form})

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
    return render(request, 'todos/edit.html', {'form': form})

def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')
    return render(request, 'todos/delete.html', {'todo_list': todo_list})
